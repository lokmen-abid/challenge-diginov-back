import mongoose from 'mongoose';

// Create the interface
export interface TaskDocument extends mongoose.Document {
  projectId:string;
  title: string;
  description: string;
  taskCreator: string;
  assignedTo: string;
  status: string;
  creationDate: Date;
  completionDate:Date;
}

// Create the schema
const TaskSchema = new mongoose.Schema({ 
  projectId : {
    type :String ,
    required:true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  taskCreator: {
    type: String,
    required: true
  },
  assignedTo: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  creationDate:{
    type: Date,
    // required: true
  },
  completionDate:{
    type: Date,
    // required: true
  }
  
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: "updatedAt"
  }
});

// Create and export task model
export const Task = mongoose.model<TaskDocument>("Task", TaskSchema);