import mongoose from 'mongoose';

// Create the interface
export interface ProjectDocument extends mongoose.Document {
  title: string;
  description: string;
  projectAdminList: string;
  projectUserList: string;
  projectStatus: string;
  projectCreationDate: Date;
}

// Create the schema
const ProjectSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  projectAdminList: {
    type: String,
    required: true
  },
  projectUserList: {
    type: String,
    required: true
  },
  projectStatus: {
    type: String,
    required: true
  },
  projectCreationDate:{
    type: Date,
    // required: true
  }
  
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: "updatedAt"
  }
});

// Create and export task model
export const Project = mongoose.model<ProjectDocument>("Project", ProjectSchema);