import mongoose from 'mongoose';

// Create the interface
export interface UserDocument extends mongoose.Document {
  name:string;
  email: string;
  password: string;
  status: string;
}

// Create the schema
const UserSchema = new mongoose.Schema({ 
  name : {
    type :String ,
    required:true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: "updatedAt"
  }
});

// Create and export task model
export const User= mongoose.model<UserDocument>("User", UserSchema);