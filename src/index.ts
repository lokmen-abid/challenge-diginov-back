import express from 'express';
import mongoose from 'mongoose';
import taskRoutes from './routes/tasks.route'
import projectRoutes from './routes/project.route'
import userRoutes from './routes/user.route'
import * as dotenv from 'dotenv';


const app = express();
dotenv.config();


mongoose.connect('mongodb://localhost:27017/diginov', { 
    // useCreateIndex: true 
    // useFindAndModify: false,
    // useNewUrlParser: true,
    // useUnifiedTopology: true,
  },() => {
    console.log("connected to mongodb");
  })    


app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if (req.method == 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
      return res.status(200).json({});
  }

  next();
});


app.use('/api/tasks/',taskRoutes)
app.use('/api/projects/',projectRoutes)
app.use('/api/user/',userRoutes)

const PORT = process.env.PORT || 4520;


app.listen(PORT, ():void => {
    console.log(`Server Running at http://localhost:${PORT}`); 
});