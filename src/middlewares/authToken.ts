
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv';
import { Request, Response, NextFunction } from 'express';


    const verification = (req:Request, res:Response, next:NextFunction) => {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
        
        if (token === null) {
            res.sendStatus(401)
        }

        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) {
                res.sendStatus(403)
            } else {
                req.user = user
            }
            next()
        })
    }