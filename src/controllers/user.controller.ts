import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import bcryptjs from 'bcryptjs';
import {User} from '../models/user' 
import jwt from 'jsonwebtoken'



const register = async (req: Request, res: Response, next: NextFunction)=>{
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already Exist')

    // hashing passwords 
    const salt = 10
    const hashedPassword = await bcryptjs.hash(req.body.password, salt)

    const user = new User({
      name:req.body.name,
      email: req.body.email,
      password: hashedPassword,
      status: req.body.status
    })

    try {
      const savedUser = await user.save();
      // res.send(savedUser);
      const token = await jwt.sign({ _id: savedUser._id }, process.env.ACCESS_TOKEN_SECRET as string);
      res.header('auth-token', token).send(JSON.stringify(token))
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
}

const login =  async (req: Request, res: Response, next: NextFunction)=>{
      // checking if the email is already exist
      const emailExist = await User.findOne({ email: req.body.email });
      if (!emailExist) return res.status(400).send('Email is wrong')
  
      // password comparison 
      const user = await User.findOne({ email: req.body.email });
  
      const validPass = await bcryptjs.compare(req.body.password, user.password)
      if (!validPass) return res.status(400).send("Invalid Password")
  
      // create and assign a token 
      const token = jwt.sign({ _id: user._id }, process.env.ACCESS_TOKEN_SECRET );
      res.header('auth-token', token).send(JSON.stringify(token))
}

const decode = async (req: Request, res: Response, next: NextFunction)=>{
    const decodedToken = jwt.decode(req.body.token, {
        complete: true
      });
      if (!decodedToken) {
        throw new Parse.Error(Parse.Error.OBJECT_NOT_FOUND, `provided token does not decode as JWT`);
      }
      console.log(decodedToken)
    //   const id = decodedToken.payload._id 
    //   const user = await User.findById(id )
    //   if (user) {
    //     res.send(JSON.stringify({ id: id, status: user.status }));
    //   } else {
    //     res.status(404).send('There is no user found with this token');
    //   }
    
}

export default { register , login ,decode };


