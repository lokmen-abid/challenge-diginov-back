import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import {Task} from '../models/tasks';

const createTask = (req: Request, res: Response, next: NextFunction) => {
    let {  projectId,
        title,
        description,
        taskCreator,
        assignedTo,
        status,
        creationDate,
        completionDate } = req.body;

    const task = new Task({
        _id: new mongoose.Types.ObjectId(),
        projectId,
        title,
        description,
        taskCreator,
        assignedTo,
        status,
        creationDate,
        completionDate
    });

    return task
        .save()
        .then((result) => {
            return res.status(201).json({
                task: result
            });
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};

const getAllTasks = (req: Request, res: Response, next: NextFunction) => {
    Task.find()
        .exec()
        .then((tasks) => {
            return res.status(200).json({
                tasks: tasks,
            });
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};



export default { createTask, getAllTasks };