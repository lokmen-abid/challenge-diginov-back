import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import {Project} from '../models/projects';

const createProject = (req: Request, res: Response, next: NextFunction) => {
    let {
        title,
        description,
        projectAdminList,
        projectUserList,
        projectStatus
     } = req.body;

    const project = new Project({
        _id: new mongoose.Types.ObjectId(),
        title,
        description,
        projectAdminList,
        projectUserList,
        projectStatus
    });

    return project
        .save()
        .then((result) => {
            return res.status(201).json({
                project: result
            });
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};

const getAllProjects = (req: Request, res: Response, next: NextFunction) => {
    Project.find()
        .exec()
        .then((projects) => {
            return res.status(200).json({
                projects: projects,
            });
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};

// const updateProject = (req:Request, res:Response, next:NextFunction)=>{
//     let id = req.params._id 
//     Project.findByIdAndUpdate()
// }

export default { createProject, getAllProjects };