import express from 'express';
import controller from '../controllers/project.controller';

const router = express.Router();

router.post('/create', controller.createProject);
router.get('/get', controller.getAllProjects);

export = router;

