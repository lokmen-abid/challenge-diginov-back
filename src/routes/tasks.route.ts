import express from 'express';
import controller from '../controllers/tasks.controller';

const router = express.Router();

router.post('/create', controller.createTask);
router.get('/get', controller.getAllTasks);

export = router;

